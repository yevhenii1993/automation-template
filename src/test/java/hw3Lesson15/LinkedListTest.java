package hw3Lesson15;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LinkedListTest {
    private List<User> users = new LinkedList<>();

    private User user = new User("First");
    private User user1 = new User("Second");
    private User user2 = new User("Third");
    private User user3 = new User("Fourth");
    private User user4 = new User("Fifth");

    @BeforeTest
    public void setUp() {
        users.add(user);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
    }

    @Test
    public void checkLinkedListTest() {

        Assert.assertFalse(users.isEmpty());

        users.print();

        Assert.assertEquals(users.size(), 5);

        users.remove(3);

        users.print();

        Assert.assertEquals(users.size(), 4);

        users.remove(user4);

        users.print();

        Assert.assertEquals(users.size(), 3);

        Assert.assertEquals(users.get(1), user1);
    }
}
